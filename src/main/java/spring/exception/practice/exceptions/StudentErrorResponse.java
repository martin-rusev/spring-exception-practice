package spring.exception.practice.exceptions;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class StudentErrorResponse {
   private int status;
   private String message;
   private long timestamp;
}
