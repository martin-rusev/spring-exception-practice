package spring.exception.practice.controllers;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import spring.exception.practice.exceptions.StudentErrorResponse;
import spring.exception.practice.exceptions.StudentNotFoundException;
import spring.exception.practice.models.Student;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/students")
public class DemoController {
    private final List<Student> students = new ArrayList<>();

    @PostConstruct
    public void initialiseStudents() {
        students.add(new Student(1,"John", "USA"));
        students.add(new Student(2,"Mary", "Italy"));
    }

    @GetMapping("/{id}")
    public Student showStudent(@PathVariable int id) {

        if (id >= students.size()) {
            throw new StudentNotFoundException("Student not found with id: " + id);
        }
        return students.get(id);
    }

    @ExceptionHandler()
    public ResponseEntity<StudentErrorResponse> handler(StudentNotFoundException exception) {
        StudentErrorResponse studentErrorResponse = new StudentErrorResponse();

        studentErrorResponse.setStatus(HttpStatus.NOT_FOUND.value());
        studentErrorResponse.setMessage(exception.getMessage());
        studentErrorResponse.setTimestamp(System.currentTimeMillis());

        return new ResponseEntity<>(studentErrorResponse, HttpStatus.NOT_FOUND);
    }

}
